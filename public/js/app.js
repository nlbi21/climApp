// Modulo de la App
var app = angular.module('climaApp', []);

// Api key OpenWeather: 2d1af937dc44ae94b2a9a60de84a8218

// Controlador
app.controller('mainCtrl', ['$scope', 'climaService', function($scope, climaService) {
    function fetchClima(city) {
        climaService.getClima(city).then(function(data) {
            $scope.place = data;
        });
    }
    fetchClima('Santiago');
    $scope.findClima = function(city) {
        $scope.place = '';
        fetchClima(city);
    };
}]);

// Servicio
app.factory('climaService', ['$http', '$q', function($http, $q) {
    function getClima(city) {
        var deferred = $q.defer();
        $http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + city + '&appid=2d1af937dc44ae94b2a9a60de84a8218&lang=es&units=metric&type=accurate')
            .success(function(data) {
                deferred.resolve(data);
            })
            .error(function(err) {
                console.log('Error...');
                deferred.reject(err);
            });
            return deferred.promise;
        }
        return {
            getClima: getClima
        };
}]);