## Resumen

Pequeña app de prueba con AngularJS en 3 horas, la cual usa información de la api open weather.

## Instalación y uso

- Wamp o xampp.
- Copiar repositorio en la carpeta www.
- Abrir el archivo index, (http://localhost/ClimaApp/).

## API

[OpenWeatherApi](https://openweathermap.org/forecast5).

## Autor
- Nelbi Alvarado